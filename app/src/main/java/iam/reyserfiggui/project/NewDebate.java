package iam.reyserfiggui.project;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class NewDebate extends AppCompatActivity {
    private EditText titleET;
    private EditText descET;
    private int maxTitleChar;
    private int maxDescChar;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_debate);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

            TextView titleTV = (TextView) findViewById(R.id.toolbar_title);
            titleTV.setText(R.string.newDebate);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        username = sharedPref.getString("username", null);

        titleET = (EditText) findViewById(R.id.titleET);
        descET = (EditText) findViewById(R.id.descET);

        maxTitleChar = Integer.parseInt(getResources().getString(R.string.max_title_char));
        maxDescChar = Integer.parseInt(getResources().getString(R.string.maxCharDesc));
        titleET.addTextChangedListener(new TextWatcher() {
            TextView maxTitleCharTV = (TextView) findViewById(R.id.maxTitleCharTV);

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int available = maxTitleChar - s.toString().length();
                maxTitleCharTV.setText(String.valueOf(available));
            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);


        descET.addTextChangedListener(new TextWatcher() {
            TextView maxCharDescTV = (TextView) findViewById(R.id.maxCharDescTV);

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int available = maxDescChar - s.toString().length();
                maxCharDescTV.setText(String.valueOf(available));
            }
        });

        FloatingActionButton saveDebate = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        saveDebate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleET.getText().toString().length() < 4)
                    Toast.makeText(getApplicationContext(), "Title too short or empty",
                            Toast.LENGTH_LONG).show();
                else
                    new SaveDebate(spinner.getSelectedItem().toString()).execute("http://reyserfiggui.tk/createDebate.php");
            }
        });
    }


    private class SaveDebate extends AsyncTask<String, String, Boolean> {

        String category;
        String title;
        String description;

        SaveDebate(String category) {
            this.category = category;
        }

        @Override
        protected void onPreExecute() {
            title = titleET.getText().toString().trim();
            description = descET.getText().toString().trim();
        }


        @Override
        protected Boolean doInBackground(String... params) {

            String urlString = params[0]; // URL to call

            try {

                URL url = new URL(urlString);

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("title", title);
                postDataParams.put("description", description);
                postDataParams.put("category", category);
                postDataParams.put("username", username);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000); // ms
                urlConnection.setConnectTimeout(15000); // ms
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            urlConnection.getInputStream()));

                    String line = in.readLine();
                    in.close();
                    return line != null && line.contains("true");
                } else {
                    return false;
                }

            } catch (Exception e) {
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean result) {
            Intent returnIntent = new Intent();
            if (result) {
                Toast.makeText(getApplicationContext(), getString(R.string.registration_complete),
                        Toast.LENGTH_LONG).show();
                returnIntent.putExtra("created", true);
                setResult(Activity.RESULT_OK, returnIntent);

            } else {
                returnIntent.putExtra("created", false);
                setResult(Activity.RESULT_CANCELED, returnIntent);
                Toast.makeText(getApplicationContext(), getString(R.string.registration_failed),
                        Toast.LENGTH_LONG).show();
            }
            finish();
        }

        String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while (itr.hasNext()) {

                String key = itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
    }
}
