package iam.reyserfiggui.project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class Debate extends Activity {
    String username;
    EditText etComment;
    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isOnline()) {
            setContentView(R.layout.no_connection_activity);
            TextView retry = (TextView) findViewById(R.id.retry);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recreate();
                }
            });
            return;
        }
        setContentView(R.layout.activity_debate);

        Intent intent = getIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        TextView debateTitle = (TextView) findViewById(R.id.debateTitle);
        TextView debateDesc = (TextView) findViewById(R.id.description);
        TextView debateAutor = (TextView) findViewById(R.id.autor);
        TextView debateCreationDate = (TextView) findViewById(R.id.creationDate);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        username = sharedPref.getString("username", null);

        String[] datos = intent.getStringArrayExtra(Intent.EXTRA_INTENT);

        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

            TextView titleTV = (TextView) findViewById(R.id.toolbar_title);
            titleTV.setText(datos[0]);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        etComment = (EditText) findViewById(R.id.etComment);
        debateTitle.setText(datos[1]);
        debateDesc.setText(datos[2]);
        debateAutor.setText(datos[3]);
        debateCreationDate.setText(datos[4]);
        id = datos[5];

        new JsonTaskComment(this, id, (LinearLayout) findViewById(R.id.llComments), 0).execute("http://reyserfiggui.tk/getDebateComments.php");

        if (username == null)
            return;
        LinearLayout llLike = (LinearLayout) findViewById(R.id.llLike);
        llLike.setVisibility(View.VISIBLE);
        LinearLayout llComment = (LinearLayout) findViewById(R.id.llComment);
        llComment.setVisibility(View.VISIBLE);
    }

    public void onClickReport(View v) {
        String[] addresses = {"a14serreylop@iam.cat", "a14guifigalb@iam.cat"};
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        String subject = "Report E-Demo ";
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void onClickSend(View v) {
        String comment = etComment.getText().toString();
        if (!comment.isEmpty())
            new JsonTaskSendComment(comment).execute("http://reyserfiggui.tk/createDebateComment.php");
        else
            Toast.makeText(v.getContext(), "Comment must have some text", Toast.LENGTH_LONG).show();
    }

    private class JsonTaskSendComment extends AsyncTask<String, String, String> {
        private String comment;

        JsonTaskSendComment(String comment) {
            this.comment = comment;
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;

            try {
                URL url = new URL(params[0]);

                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setReadTimeout(15000); // ms
                connection.setConnectTimeout(15000); // ms
                connection.setDoOutput(true);
                connection.setDoInput(true);

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                String param = URLEncoder.encode("idDebate", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8") + "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" + URLEncoder.encode("comment", "UTF-8") + "=" + URLEncoder.encode(comment.trim(), "UTF-8");

                writer.write(param);
                writer.flush();
                writer.close();
                os.close();

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            connection.getInputStream()));

                    String line = in.readLine();

                    in.close();

                    return line;
                } else {
                    return String.valueOf(responseCode);
                }


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("true")) {
                recreate();
                etComment.setText(null);
            }
        }
    }
}
