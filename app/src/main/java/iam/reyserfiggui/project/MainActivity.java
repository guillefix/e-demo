package iam.reyserfiggui.project;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import iam.reyserfiggui.project.fragments.DebatesFragment;
import iam.reyserfiggui.project.fragments.ProposalsFragment;
import iam.reyserfiggui.project.fragments.StadisticsFragment;
import iam.reyserfiggui.project.fragments.VotingFragment;
import iam.reyserfiggui.project.settings.Settings;


public class MainActivity extends AppCompatActivity {
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    static TabLayout tabLayout;
    ViewPager viewPager;
    static SearchView searchView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    int int_items = 4;
    AppCompatImageButton newButton;
    AppCompatImageButton filterButton;
    PopupMenu popupMenu;
    String username;
    SharedPreferences sharedPref;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("themeSwitcher", false)) {
            setTheme(R.style.AppThemeDark);
/*            if(mNavigationView!=null){
                mNavigationView.setItemTextColor(ColorStateList.valueOf(Color.WHITE));
                mNavigationView.setItemIconTintList(null);
            }*/
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
                *Setup the DrawerLayout and NavigationView
                */
                newButton = (AppCompatImageButton) findViewById(R.id.newButton);
        filterButton = (AppCompatImageButton) findViewById(R.id.filter);

        popupMenu = new PopupMenu(MainActivity.this, filterButton);
        popupMenu.getMenuInflater().inflate(R.menu.menu_filter, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.setChecked(!item.isChecked());
                return false;
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navView);

        searchView = (SearchView) findViewById(R.id.search);


        tabLayout = (TabLayout) findViewById(R.id.tabs);

        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);

        int width = metrics.widthPixels;

//            Toast.makeText(view.getContext(), String.valueOf(width), Toast.LENGTH_LONG).show();
        if (width < 1090) {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        /*


         Set an Apater for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));

        /*
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.viewpager, new DebatesFragment()).commit();

        /**
         * Setup click events on the Navigation View Items.
         */
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        alertDialogBuilder.setTitle(R.string.help);
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        alertDialogBuilder.setPositiveButton("OK", null);
        dialog = alertDialogBuilder.create();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            dialog.setMessage(Html.fromHtml("<Big>" + getString(R.string.help_description) + "</Big>" + "<br/><br/>" + getString(R.string.rate_app), Html.FROM_HTML_MODE_LEGACY));
        else
            dialog.setMessage(Html.fromHtml("<Big>" + getString(R.string.help_description) + "</Big>" + "<br/><br/>" + getString(R.string.rate_app)));

        AlertDialog.Builder adBuilder = new AlertDialog.Builder(
                this);
        // set title
        adBuilder.setTitle(R.string.logout);
        // set dialog message
        adBuilder
                .setMessage(R.string.logout_msg)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        logout();
                    }
                })
                .setNegativeButton(R.string.No, null);

        // create alert dialog
        final AlertDialog alertDialog = adBuilder.create();

        // show it

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
/*
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

                if (menuItem.getItemId() == R.id.nav_home) {
                }*/
                if (menuItem.getItemId() == R.id.nav_profile) {
                    if (username != null) {
                        Intent intent = new Intent(MainActivity.this, MyProfile.class);
                        startActivity(intent);
                    } else {
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        startActivityForResult(i, 1);
                    }

                }
                if (menuItem.getItemId() == R.id.nav_settings) {
                    startActivity(new Intent(getApplicationContext(), Settings.class));
                }
                if (menuItem.getItemId() == R.id.nav_share) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                    String sAux = "Descarga la nueva app E-Demo gratis!\n\nhttps://play.google.com/store/apps/details?id=" + getPackageName();
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, null));
                }
                if (menuItem.getItemId() == R.id.nav_report) {
                    String[] addresses = {"a14serreylop@iam.cat", "a14guifigalb@iam.cat"};
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, addresses);
                    String subject = "Report E-Demo ";
                    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
                if (menuItem.getItemId() == R.id.nav_rate) {
                    launchMarket();
                }
                if (menuItem.getItemId() == R.id.nav_help) {
                    dialog.show();
                }
                if (menuItem.getItemId() == R.id.nav_logout) {
                    if (username != null)
                        alertDialog.show();
                    else
                        logout();
                }
                return false;
            }

        });
        /**
         * Setup Drawer Toggle of the Toolbar
         */

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);


        mDrawerToggle.syncState();
        if (sharedPref == null)
            loggedIn();

    }

    private void loggedIn() {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if ((username = sharedPref.getString("username", null)) != null) {
            newButton.setVisibility(View.VISIBLE);
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (tab.getPosition() == 0 || tab.getPosition() == 1) {
                        if (newButton.getVisibility() == View.GONE)
                            newButton.setVisibility(View.VISIBLE);
                        newButton.animate().rotation(360).setDuration(500).alpha(1.0f);
                    } else {
                        newButton.setVisibility(View.GONE);
                        newButton.setRotation(0);
                        newButton.setAlpha(0.0f);
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            Menu menu = mNavigationView.getMenu();
            // find MenuItem you want to change
            MenuItem nav_profile = menu.findItem(R.id.nav_profile);
            // set new title to the MenuItem
            nav_profile.setTitle(R.string.profile);

            MenuItem nav_exit = menu.findItem(R.id.nav_logout);

            nav_exit.setTitle(R.string.logout);
        }
    }

    private void logout() {
        if (username != null) {
            username = null;
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("username", null);
            editor.apply();

            Snackbar.make(mDrawerLayout, R.string.logout_success_info, Snackbar.LENGTH_LONG).show();
            recreate();
        } else {
            onBackPressed();
        }
    }

    public void onClickNew(View v) {
        Intent intent = null;
        int tabPos = getTabPos();
        switch (tabPos) {
            case 0:
                intent = new Intent(MainActivity.this, NewDebate.class);
                break;
            case 1:
                intent = new Intent(MainActivity.this, NewProposal.class);
                break;
        }
        startActivityForResult(intent, 0);
    }


    public void onClickFilter(View v) {
        popupMenu.show();
    }


    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    public static void onSearchBarTextChange() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return true;
            }


            @Override
            public boolean onQueryTextChange(String query) {
                switch (getTabPos()) {
                    case 0:
                        DebatesFragment.listDebatesAdapter.getFilter().filter(query);
                        break;
                    case 1:
                        ProposalsFragment.listProposalsAdapter.getFilter().filter(query);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                loggedIn();
            }
        } else if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                recreate();
            }
        }
    }//onActi

    private class MyAdapter extends FragmentPagerAdapter {
        String[] tabsName = getResources().getStringArray(R.array.tabsName);

        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new DebatesFragment();
                case 1:
                    return new ProposalsFragment();
                case 2:
                    return new VotingFragment();
                case 3:
                    return new StadisticsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {
            return tabsName[position];
        }
    }

    public static int getTabPos() {
        return tabLayout.getSelectedTabPosition();
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setQuery(null, true);
            searchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }
}