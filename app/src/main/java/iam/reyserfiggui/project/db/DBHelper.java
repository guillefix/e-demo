package iam.reyserfiggui.project.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * Crea un debate y lo inserta en la base de datos
     * Solo se puede hacer por usuarios registrados
     *
     * @param name título del debate
     * @param description descripción sobre el debate
     * @param category categoria del debate (social, democracia, económico, sanitario...)
     * @return
     */
    public boolean createDebate (String name, String description, String category ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("description", description);
        contentValues.put("category", category);

        db.insert("debates", null, contentValues);
        return true;
    }


    /**
     * Recupera la lista de debates (solo el título)
     *
     * @return arraylist con los títulos
     */
    public ArrayList<String> getAllDebates() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from debates", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex("name")));
            res.moveToNext();
        }

        return array_list;
    }

    /**
     * Elimina un debate
     * Solo se puede usar por administradores
     *
     * @param id identificador del debate a eliminar
     * @return
     */
    public Integer deleteDebate (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("debates", "id = ? ", new String[] { Integer.toString(id) });
    }

}
