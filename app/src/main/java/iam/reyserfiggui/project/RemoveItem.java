package iam.reyserfiggui.project;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sergio on 31/05/2017.
 * Nothing else to add
 */

public class RemoveItem implements Runnable {
    private URL url;
    private String id;
    private Context context;
    private int type;

    public RemoveItem(String url, String id, Context context, int type) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.id = id;
        this.context = context;
        this.type = type;
    }

    @Override
    public void run() {
        try {

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(15000); // ms
            urlConnection.setConnectTimeout(15000); // ms
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            String idType;
            if (type == 0) {
                idType = "idDebate";
            } else
                idType = "idProposal";
            String param = URLEncoder.encode(idType, "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8");
            writer.write(param);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = urlConnection.getResponseCode();
            Log.d("RESPONSE CODE", String.valueOf(responseCode) + "  " + id);
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new
                        InputStreamReader(
                        urlConnection.getInputStream()));

                String line = in.readLine();
                in.close();
                if (line != null)
                    Log.d("CACA", line);
//                    Toast.makeText(context, line, Toast.LENGTH_LONG).show();
                else
                    Log.d("CACA", "ES NULL");
//                    Toast.makeText(context, "Es null", Toast.LENGTH_LONG).show();
            } else {
                Log.d("CACA", "NO HTTP");
//                Toast.makeText(context, "NO HTTP OK", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
//            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }
}
