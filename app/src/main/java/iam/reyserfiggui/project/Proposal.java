package iam.reyserfiggui.project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class Proposal extends AppCompatActivity {

    private List<String[]> arrayComments;
    //    ListView listComments;
    LinearLayout ll;
    String id;
    TextView proposalSupports;
    EditText etComment;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isOnline()) {
            setContentView(R.layout.no_connection_activity);
            TextView retry = (TextView) findViewById(R.id.retry);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recreate();
                }
            });
            return;
        }
        setContentView(R.layout.activity_proposal);

        Intent intent = getIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        TextView proposalTitle = (TextView) findViewById(R.id.debateTitle);
        TextView proposalDesc = (TextView) findViewById(R.id.description);
        TextView proposalAutor = (TextView) findViewById(R.id.autor);
        TextView proposalCreationDate = (TextView) findViewById(R.id.creationDate);
        proposalSupports = (TextView) findViewById(R.id.supports);
        TextView proposalLimitDate = (TextView) findViewById(R.id.limitDateValue);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        username = sharedPref.getString("username", null);
        String[] datos = intent.getStringArrayExtra(Intent.EXTRA_INTENT);

        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

            TextView titleTV = (TextView) findViewById(R.id.toolbar_title);
            titleTV.setText(datos[0]);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        proposalTitle.setText(datos[1]);
        proposalDesc.setText(datos[2]);
        proposalAutor.setText(datos[3]);
        proposalCreationDate.setText(datos[4]);
        proposalSupports.setText(datos[5]);
        proposalLimitDate.setText(datos[6]);
        id = datos[7];

        arrayComments = new ArrayList<>();

        new JsonTaskComment(this, id, (LinearLayout) findViewById(R.id.llComments), 1).execute("http://reyserfiggui.tk/getProposalComments.php");

        if (username == null)
            return;

        etComment = (EditText) findViewById(R.id.etComment);
        Button supportButton = (Button) findViewById(R.id.support);
        LinearLayout llComment = (LinearLayout) findViewById(R.id.llComment);
        llComment.setVisibility(View.VISIBLE);
        supportButton.setVisibility(View.VISIBLE);

    }

    public void onClickSupport(View v) {
        new JsonTaskSupport().execute("http://reyserfiggui.tk/supportProposal.php");
    }

    public void onClickSend(View v) {
        String comment = etComment.getText().toString();
        if (!comment.isEmpty())
            new JsonTaskSendComment(comment).execute("http://reyserfiggui.tk/createProposalComment.php");
        else
            Toast.makeText(v.getContext(), "Comment must have some text", Toast.LENGTH_LONG).show();
    }

    /**
     * Obtiene los debates existentes de la BBDD de forma asíncrona
     */


    public void onClickReport(View v) {
        String[] addresses = {"a14serreylop@iam.cat", "a14guifigalb@iam.cat"};
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        String subject = "Report E-Demo ";
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private class JsonTaskSupport extends AsyncTask<String, String, String> {

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);

                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setReadTimeout(15000); // ms
                connection.setConnectTimeout(15000); // ms
                connection.setDoOutput(true);
                connection.setDoInput(true);

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                String param = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" + URLEncoder.encode("idProposal", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8");

                writer.write(param);
                writer.flush();
                writer.close();
                os.close();

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            connection.getInputStream()));

                    String line = in.readLine();

                    in.close();

                    return line;
                } else {
                    return String.valueOf(responseCode);
                }


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("true")) {
                proposalSupports.setText(String.valueOf(Integer.parseInt(proposalSupports.getText().toString().trim()) + 1));
            }
        }
    }

    private class JsonTaskSendComment extends AsyncTask<String, String, String> {
        private String comment;

        public JsonTaskSendComment(String comment) {
            this.comment = comment;
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;

            try {
                URL url = new URL(params[0]);

                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setReadTimeout(15000); // ms
                connection.setConnectTimeout(15000); // ms
                connection.setDoOutput(true);
                connection.setDoInput(true);

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                String param = URLEncoder.encode("idProposal", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8") + "&" + URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" + URLEncoder.encode("comment", "UTF-8") + "=" + URLEncoder.encode(comment.trim(), "UTF-8");

                writer.write(param);
                writer.flush();
                writer.close();
                os.close();

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            connection.getInputStream()));

                    String line = in.readLine();

                    in.close();

                    return line;
                } else {
                    return String.valueOf(responseCode);
                }


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("true")) {
                recreate();
                etComment.setText(null);
            }
        }
   /* public class CustomAdapter extends BaseAdapter {

        private List<String[]> commentsArrayList;
        Context _c;

        CustomAdapter(List<String[]> data, Context c) {
            commentsArrayList = data;
            _c = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return commentsArrayList.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return commentsArrayList.get(position);
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.comment_list_item, null);
            }

            TextView fromView = (TextView) v.findViewById(R.id.From);
            TextView descView = (TextView) v.findViewById(R.id.description);
            TextView timeView = (TextView) v.findViewById(R.id.time);

            String[] msg = commentsArrayList.get(position);
            fromView.setText(msg[1]);
            descView.setText(msg[0]);
            timeView.setText(msg[2]);

            return v;
        }
    }*/
    }
}
