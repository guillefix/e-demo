package iam.reyserfiggui.project;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergio on 01/06/2017.
 * Nothing else to add
 */

class JsonTaskComment extends AsyncTask<String, String, String> {
    private Context context;
    private String id;
    private LinearLayout ll;
    private int type;
    private List<String[]> arrayComments = new ArrayList<>();

    JsonTaskComment(Context context, String id, LinearLayout linearLayout, int type) {
        this.context = context;
        this.id = id;
        this.ll = linearLayout;
        this.type = type;
    }
    // JSON

    protected String doInBackground(String... params) {


        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(params[0]);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setReadTimeout(15000); // ms
            connection.setConnectTimeout(15000); // ms
            connection.setDoOutput(true);
            connection.setDoInput(true);

            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            String typeObj;
            if (type == 1) {
                typeObj = "idProposal";
            } else
                typeObj = "idDebate";
            String param = URLEncoder.encode(typeObj, "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8");

            writer.write(param);
            writer.flush();
            writer.close();
            os.close();

            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuilder buffer = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                Log.d("LINE :", line);
                buffer.append(line).append("\n");
            }

            return buffer.toString();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        JSONObject object = null; //Creamos un objeto JSON a partir de la cadena
        try {
            object = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        assert object != null;
        JSONArray json_array = object.optJSONArray("comments"); //cogemos cada uno de los elementos dentro de la etiqueta "comments"
        if (json_array == null) {
            return;
        }
        for (int i = 0; i < json_array.length(); i++) {
            try {
                String[] data = {json_array.getJSONObject(i).getString("comment"), json_array.getJSONObject(i).getString("username"), json_array.getJSONObject(i).getString("date")};

                arrayComments.add(0, data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (String[] comment : arrayComments) {
            View v = vi.inflate(R.layout.comment_list_item, null);


            TextView fromView = (TextView) v.findViewById(R.id.From);
            TextView descView = (TextView) v.findViewById(R.id.description);
            TextView timeView = (TextView) v.findViewById(R.id.time);

            String[] msg = comment;
            fromView.setText(msg[1]);
            descView.setText(msg[0]);
            timeView.setText(msg[2]);

            ll.addView(v);
        }

//                listComments.setAdapter(new CustomAdapter(arrayComments, context));

    }
}
