package iam.reyserfiggui.project;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class Register extends AppCompatActivity {

    private EditText name;
    private EditText surname;
    private EditText username;
    private EditText emailET;
    private EditText passwordET;
    private EditText passwordConfET;
    private EditText nif;
    private EditText tlf;
    private AutoCompleteTextView municipality;
    private RadioGroup genderRadioGroup;
    private EditText bornDate;

    // Strings que contienen el valor de los edittext.getText()
    private String nameValue;
    private String surnameValue;
    private String usernameValue;
    private String emailValue;
    private String nifValue;
    private String tlfValue;
    private String municipalityValue;
    private String genderValue;
    private String bornDateValue;

    private String password;
    private static final int SELECT_PICTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

            TextView titleTV = (TextView) findViewById(R.id.toolbar_title);
            titleTV.setText(R.string.action_sign_in_short);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        name = (EditText) findViewById(R.id.name);
        surname = (EditText) findViewById(R.id.surname);
        username = (EditText) findViewById(R.id.username);
        nif = (EditText) findViewById(R.id.nif);
        tlf = (EditText) findViewById(R.id.tlf);
        municipality = (AutoCompleteTextView) findViewById(R.id.city);

        genderRadioGroup = (RadioGroup) findViewById(R.id.genderSelect);

        emailET = (EditText) findViewById(R.id.email);
        passwordET = (EditText) findViewById(R.id.password);
        passwordConfET = (EditText) findViewById(R.id.passwordConfirmation);
        bornDate = (EditText) findViewById(R.id.bornDate);
        bornDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DialogFragment newFragment = new DatePickerFragment();
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                }

            }

        });
        Intent intent = getIntent();


        usernameValue = intent.getStringExtra("username");
        password = intent.getStringExtra("password");

        if (usernameValue != null)
            username.setText(usernameValue);
        if (password != null)
            passwordET.setText(password);

        setCities(municipality);
        if (!isOnline()) {
            LinearLayout ll = (LinearLayout) findViewById(R.id.linear_layout_register);
            Snackbar.make(ll, R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
        }
    }

    public void setCities(AutoCompleteTextView autoCompleteTextView) {
        String[] spanishCities = {"Madrid", "Barcelona", "Valencia", "Sevilla", "Málaga", "Cádiz", "Alicante",
                "Zaragoza", "Vizcaya", "Murcia", "Cantabria", "Lugo", "Palma de Mallorca", "Granada", "Badajoz", "Badajoz", "A Coruña", "Ourense",
                "Orense", "La Coruña", "Pontevedra", "Burgos", "La Rioja", "Toledo", "Navarra", "Almería", "Huelva", "Córdoba", "Salamanca", "Asturias", "León",
                "Guipúzcoa", "Álava", "Castellón", "Granada", "Cáceres", "Ciudad Real", "Cuenca", "Albacete", "Teruel", "Huesca",
                "Zamora", "Palencia", "Segovia", "Valladolid", "Ávila", "Soria", "Guadalajara", "Jaén", "Santa Cruz",
                "Las Palmas", "Islas Baleares", "Tarragona", "Gerona", "Lérida", "Ceuta", "Melilla"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, spanishCities);
        autoCompleteTextView.setAdapter(adapter);
    }

    public void pickDate(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void getProfilePic(View v) {


        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
        chooserIntent.putExtra
                (
                        Intent.EXTRA_INITIAL_INTENTS,
                        new Intent[]{takePhotoIntent}
                );

        startActivityForResult(chooserIntent, SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SELECT_PICTURE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ImageButton profilePic = (ImageButton) findViewById(R.id.profPic);
                profilePic.setImageURI(data.getData());
            }
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            EditText bornDate = (EditText) getActivity().findViewById(R.id.bornDate);
            String date = day + "/" + (month + 1) + "/" + year;
            bornDate.setText(date);
        }
    }

    public void onClickRegister(View v) {
        if (!isOnline()) {
            LinearLayout ll = (LinearLayout) findViewById(R.id.linear_layout_register);
            Snackbar.make(ll, R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
            return;
        }
        password = passwordET.getText().toString();
        String passwordConf = passwordConfET.getText().toString();
        Boolean correctValues = true;

        emailValue = emailET.getText().toString();
        tlfValue = tlf.getText().toString();
        int selectedId = genderRadioGroup.getCheckedRadioButtonId();
        if (selectedId != -1) {
            RadioButton genderRadioButton = (RadioButton) findViewById(selectedId);
            genderValue = genderRadioButton.getText().toString();
        } else {
            LinearLayout ll = (LinearLayout) findViewById(R.id.linear_layout_register);
            Snackbar.make(ll, "Gender empty value", Snackbar.LENGTH_LONG).show();
            correctValues = false;
        }
        if (password.isEmpty() || !password.equals(passwordConf)) {
            setError("Las contraseñas no coinciden", passwordConfET);
            correctValues = false;
        }
        if ((nameValue = name.getText().toString()).isEmpty()) {
            setError(null, name);
            correctValues = false;
        }

        if ((surnameValue = surname.getText().toString()).isEmpty()) {
            setError(null, surname);
            correctValues = false;
        }

        if ((usernameValue = username.getText().toString()).isEmpty()) {
            setError(null, username);
            correctValues = false;
        }

        if ((nifValue = nif.getText().toString()).isEmpty()) {
            setError(null, nif);
            correctValues = false;
        }

        if ((municipalityValue = municipality.getText().toString()).isEmpty()) {
            setError(null, municipality);
            correctValues = false;
        }

        if ((bornDateValue = bornDate.getText().toString()).isEmpty()) {
            setError(null, bornDate);
            correctValues = false;
        }


        if (correctValues)
            new RegisterPHP().execute("http://reyserfiggui.tk/register.php");
    }


    public void setError(String message, EditText editText) {
        if (message == null)
            editText.setError("Campo vacio");
        else
            editText.setError(message);
    }

    // https://www.studytutorial.in/android-httpurlconnection-post-and-get-request-tutorial
    private class RegisterPHP extends AsyncTask<String, Object, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            String urlString = params[0]; // URL to call


            try {

                URL url = new URL(urlString);

                // Parametros POST
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("name", nameValue);
                postDataParams.put("surname", surnameValue);
                postDataParams.put("username", usernameValue);
                postDataParams.put("password", password);
                postDataParams.put("email", emailValue);
                postDataParams.put("nif", nifValue);
                postDataParams.put("phone", tlfValue);
                postDataParams.put("municipality", municipalityValue);
                postDataParams.put("bornDate", bornDateValue);
                postDataParams.put("gender", genderValue);
                Log.e("params", postDataParams.toString());

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setReadTimeout(15000); // ms
                urlConnection.setConnectTimeout(15000); // ms
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                String postData = getPostDataString(postDataParams);
                writer.write(postData);

                writer.flush();
                writer.close();
                os.close();

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            urlConnection.getInputStream()));

                    String line = in.readLine();
                    in.close();
                    return line != null && line.equals("true");
                } else {
                    return false;
                }

            } catch (Exception e) {

                Log.d("EXCEPTION REGISTER", e.getMessage());

                return false;

            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(getApplicationContext(), getString(R.string.registration_complete),
                        Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.registration_failed),
                        Toast.LENGTH_LONG).show();
            }
        }

        String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while (itr.hasNext()) {

                String key = itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}