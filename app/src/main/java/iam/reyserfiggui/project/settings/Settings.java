package iam.reyserfiggui.project.settings;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.widget.Toast;

import iam.reyserfiggui.project.R;

/**
 * Created by Sergio on 28/03/2017.
 * Nothing else to add
 */

public class Settings extends PreferenceActivity {
    SwitchPreference theme_switcher;
    boolean theme = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("themeSwitcher", false))
            setTheme(R.style.AppThemeDark);

        getApplicationContext().setTheme(R.style.AppThemeDark);
        theme = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("themeSwitcher", false);
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);

        if (theme_switcher == null) {
            theme_switcher = (SwitchPreference) findPreference("themeSwitcher");
            final AlertDialog ad = setAlertDialog();
            theme_switcher.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    //PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("themeSwitcher", false)

                    if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("themeSwitcher", false) != theme)
                        ad.show();
                    else
                        Toast.makeText(getApplicationContext(), R.string.theme_reverted, Toast.LENGTH_SHORT).show();
                    return true;

                }
            });
        }
    }

    private AlertDialog setAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        alertDialogBuilder.setTitle(R.string.theme_title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            alertDialogBuilder.setMessage(Html.fromHtml("<Big>" + getString(R.string.theme_msg) + "<br/><br/>" + getString(R.string.theme_restart_question) + "</Big>", Html.FROM_HTML_MODE_LEGACY));
        else
            alertDialogBuilder.setMessage(Html.fromHtml("<Big>" + getString(R.string.theme_msg) + "<br/><br/>" + getString(R.string.theme_restart_question) + "</Big>"));
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(getApplicationContext(), R.string.change_theme_info, Toast.LENGTH_SHORT).show();
            }
        });
        return alertDialogBuilder.create();
    }
}