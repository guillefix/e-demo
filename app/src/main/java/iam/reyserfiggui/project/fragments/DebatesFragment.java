package iam.reyserfiggui.project.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import iam.reyserfiggui.project.Debate;
import iam.reyserfiggui.project.MainActivity;
import iam.reyserfiggui.project.R;
import iam.reyserfiggui.project.RemoveItem;


public class DebatesFragment extends Fragment {

    /*
    private static final String TAG_RESULTS="result";
    private static final String TAG_TITLE = "title";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_CATEGORY ="category";*/

    private List<String[]> arrayDebates;
    public static ArrayAdapter<String> listDebatesAdapter;
    ListView listDebates;
    SwipeRefreshLayout swipeRefreshLayout;
    String usernameSharedPref;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!isOnline()) {
            view = inflater.inflate(R.layout.no_connection_activity, container, false);
            TextView retry = (TextView) view.findViewById(R.id.retry);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().recreate();
                }
            });
            return view;
        }
            view = inflater.inflate(R.layout.debates_fragment, container, false);
            arrayDebates = new ArrayList<>();
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
            swipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            if (isOnline()) {
                                arrayDebates.clear();
                                new JsonTask().execute("http://reyserfiggui.tk/getDebates.php");
                            } else {
                                swipeRefreshLayout.setRefreshing(false);
                                getActivity().recreate();
                            }
                        }
                    }
            );

        new JsonTask().execute("http://reyserfiggui.tk/getDebates.php");

        return view;
    }

    /**
     * Obtiene los debates existentes de la BBDD de forma asíncrona
     */
    private class JsonTask extends AsyncTask<String, String, String> {
        // JSON
        ProgressDialog pd;

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.recoveringDebates));
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuilder buffer = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }

                return buffer.toString();


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pd.isShowing()) {
                pd.dismiss();
            }

            JSONObject object = null; //Creamos un objeto JSON a partir de la cadena
            try {
                object = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            assert object != null;
            JSONArray json_array = object.optJSONArray("debates"); //cogemos cada uno de los elementos dentro de la etiqueta "debates"
            for (int i = 0; i < json_array.length(); i++) {
                    try {
                        String[] data = {json_array.getJSONObject(i).getString("category"), json_array.getJSONObject(i).getString("title"), json_array.getJSONObject(i).getString("description"), json_array.getJSONObject(i).getString("username"), json_array.getJSONObject(i).getString("creationDate").substring(0, 10), json_array.getJSONObject(i).getString("id")};

                        arrayDebates.add(0, data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                listDebates = (ListView) view.findViewById(R.id.listDebates);

                final List<String> titles = new ArrayList<>();
                for (String[] title : arrayDebates) {
                    titles.add(title[1]);
                }
                if (getActivity() != null) {
                    listDebatesAdapter = new ArrayAdapter<>(
                            getActivity(),
                            android.R.layout.simple_list_item_1,
                            titles
                    );
                    listDebates.setAdapter(listDebatesAdapter);
                    listDebates.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position,
                                                long id) {
                            Intent intent = new Intent(getActivity(), Debate.class);
                            intent.putExtra(Intent.EXTRA_INTENT, arrayDebates.get(position));
                            startActivity(intent);
                        }
                    });

                    listDebates.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                            if (usernameSharedPref == null)
                                return false;
                            if (!usernameSharedPref.equals(arrayDebates.get(position)[3])) {
                                return false;
                            }
                            PopupMenu popup = new PopupMenu(parent.getContext(), view);
                            popup.getMenuInflater().inflate(R.menu.menu_list_item, popup.getMenu());
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem menuItem) {
                                    listDebatesAdapter.remove(listDebatesAdapter.getItem(position));
                                    arrayDebates.remove(position);
                                    new Thread(new RemoveItem("http://reyserfiggui.tk/deleteDebate.php", arrayDebates.get(position)[5], view.getContext(), 0)).start();
                                    return true;
                                }
                            });
                            popup.show();
                            return true;
                        }
                    });

                }
            swipeRefreshLayout.setRefreshing(false);
            MainActivity.onSearchBarTextChange();
        }
    }

    @Override
    public void onResume() {
        usernameSharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("username", null);
        super.onResume();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
