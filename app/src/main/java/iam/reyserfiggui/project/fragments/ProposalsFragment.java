package iam.reyserfiggui.project.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import iam.reyserfiggui.project.Proposal;
import iam.reyserfiggui.project.R;
import iam.reyserfiggui.project.RemoveItem;


public class ProposalsFragment extends Fragment {

    private List<String> arrayProposals;
    public static ArrayAdapter<String> listProposalsAdapter;
    ListView listProposals;
    SwipeRefreshLayout swipeRefreshLayout;
    String username;
    JSONArray json_array;
    View view;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if (!isOnline()) {
            view = inflater.inflate(R.layout.no_connection_activity, container, false);
            TextView retry = (TextView) view.findViewById(R.id.retry);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().recreate();
                }
            });
            return view;
        }
        view = inflater.inflate(R.layout.proposals_fragment, container, false);

        arrayProposals = new ArrayList<>();

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (isOnline()) {
                            arrayProposals.clear();
                            new JsonTask().execute("http://reyserfiggui.tk/getProposals.php");
                        } else {
                            swipeRefreshLayout.setRefreshing(false);
                            getActivity().recreate();
                        }
                    }
                }
        );
        new JsonTask().execute("http://reyserfiggui.tk/getProposals.php");

        return view;
    }

    /**
     * Obtiene los debates existentes de la BBDD de forma asíncrona
     */
    private class JsonTask extends AsyncTask<String, String, String> {
        // JSON
        ProgressDialog pd;

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.recoveringDebates));
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuilder buffer = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }

                return buffer.toString();


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pd.isShowing()) {
                pd.dismiss();
            }

            JSONObject object = null; //Creamos un objeto JSON a partir de la cadena
            try {
                object = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            assert object != null;
            json_array = object.optJSONArray("proposals"); //cogemos cada uno de los elementos dentro de la etiqueta "debates"

            for (int i = 0; i < json_array.length(); i++) {
                try {
                    arrayProposals.add(json_array.getJSONObject(i).getString("title"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            listProposals = (ListView) view.findViewById(R.id.listProposals);

            if (getActivity() != null) {
                listProposalsAdapter = new ArrayAdapter<>(
                        getActivity(),
                        android.R.layout.simple_list_item_1,
                        arrayProposals
                );

                listProposals.setAdapter(listProposalsAdapter);
                listProposals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        try {
                            String[] proposal = {json_array.getJSONObject(position).getString("category"), arrayProposals.get(position), json_array.getJSONObject(position).getString("description"), json_array.getJSONObject(position).getString("username"), json_array.getJSONObject(position).getString("creationDate").substring(0, 10), json_array.getJSONObject(position).getString("supports"), json_array.getJSONObject(position).getString("limitDate"), json_array.getJSONObject(position).getString("id")};
                            Intent intent = new Intent(getActivity(), Proposal.class);
                            intent.putExtra(Intent.EXTRA_INTENT, proposal);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                listProposals.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                        if (username == null)
                            return false;
                        try {
                            if (!username.equals(json_array.getJSONObject(position).getString("username"))) {
                                return false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        PopupMenu popup = new PopupMenu(parent.getContext(), view);
                        popup.getMenuInflater().inflate(R.menu.menu_list_item, popup.getMenu());
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                listProposalsAdapter.remove(listProposalsAdapter.getItem(position));
                                try {
                                    new Thread(new RemoveItem("http://reyserfiggui.tk/deleteProposal.php", json_array.getJSONObject(position).getString("id"), view.getContext(), 1)).start();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }
                        });
                        popup.show();
                        return true;
                    }
                });
            }
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onResume() {
        username = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getString("username", null);
        super.onResume();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
